set nocompatible

" Set the location of the swapfiles
set directory=$XDG_CACHE_HOME/vim/swap//

" Set the directory to use with :set backup=on
set backupdir=.,$XDG_CACHE_HOME/vim

" Set the location to save vim state to
set viminfofile=$XDG_CACHE_HOME/vim/viminfo

" Set the load order for runtime files
set runtimepath=$VIM,$VIMRUNTIME,$XDG_CONFIG_HOME/vim,$XDG_CONFIG_HOME/vim/after

let $MYVIMRC=$XDG_CONFIG_HOME."/vim/vimrc"
let $PLUGDIR=$XDG_CONFIG_HOME."/vim/plugged"

" Make sure our environment is setup correctly
if !isdirectory($XDG_CACHE_HOME . "vim/undo")
	call mkdir($XDG_CACHE_HOME . "vim/undo", "p", 0700)
endif
if !isdirectory($XDG_CACHE_HOME . "vim/swap")
	call mkdir($XDG_CACHE_HOME . "vim/swap", "p", 0700)
endif
if !isdirectory($PLUGDIR)
	call mkdir($PLUGDIR, "p", 0700)
endif

" Plugin configuration
if empty(glob($XDG_CONFIG_HOME.'/vim/autoload/plug.vim'))
	silent !curl -fLo "$XDG_CONFIG_HOME/vim/autoload/plug.vim" --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
else
	call plug#begin($PLUGDIR)
	Plug 'tpope/vim-surround'
	Plug 'tpope/vim-fugitive'
	Plug 'plasticboy/vim-markdown'
	Plug 'ledger/vim-ledger'
	Plug 'altercation/vim-colors-solarized'
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'
	call plug#end()
endif

" Plugin settings
let g:airline_powerline_fonts = 1
let g:airline_theme='dark'

" Enable persistent undo if supported
if has('persistent_undo')
	set undodir=$XDG_CACHE_HOME"/vim/undo"
	set undofile
endif

" Now we can turn our filetype functionality back on
filetype plugin indent on

" Enable solarized color scheme
set background=dark
colorscheme solarized

" Disable Background Color Erase (BCE) so that color schemes
" render properly when inside 256-color tmux and GNU screen.
" see also http://snk.tuxfamily.org/log/vim-256color-bce.html
if &term =~ '256color'
	set t_ut=
endif

" Enable relative line numbering
set relativenumber

" Show a line at the 80 character mark
set colorcolumn=80

" Enables mouse support
set mouse=a

" Enables syntax highlighting
syntax on

" Enables line numbers
set number

" Highlight trailing spaces
match ErrorMsg '\s\+$'

" Show the column the cursor is in
set cursorcolumn

" Show the line the cursor is on
set cursorline

" Set the leader to <Space>
let mapleader = "\<Space>"

" Some easy ways to get out of insert mode
inoremap jk <Esc>
inoremap kj <Esc>

" Key Mappings
noremap <F7> :tabp<Enter>
noremap <F8> :tabn<Enter>

" Explicitly set the tab behavoiur
set tabstop=8
set softtabstop=0
set noexpandtab
set shiftwidth=8

" Save the file, even if I am still pressing shift
command W w

" Setup a "IDE" mode
function SetupIDE()
        let g:netrw_banner = 0          " Turn off the banner in netrw
        let g:netrw_liststyle = 3       " Set the preffered list style
        let g:netrw_browse_split = 4    " Open files in one split
        let g:netrw_winsize = 12        " Set window size
        Vexplore
endfunction

" Define a command to setup "IDE" mode
command IDE call SetupIDE()

" Map <Leader>I to enter "IDE" mode
noremap <Leader>I :IDE<Enter>

" Map <Leader>w to write the current buffer
noremap <Leader>w :write<Enter>

" Map <Leader>q to save and quit
noremap <Leader>q :wq<Enter>

" Map <Leader>Q to quit immediately
noremap <Leader>Q :q!<Enter>

" Map <Leader>i to show the current buffers
noremap <Leader>i :buffers<Enter>

" Map <Leader>l to move forward a buffer
noremap <Leader>j :bnext<Enter>

" Map <Leader>h to move back a buffer
noremap <Leader>k :bprevious<Enter>

" Map <Leader>h to use xxd to render a hexdump of the file
noremap <Leader>h :%!xxd -c 32<Enter>

" Map <Leader>h to use xxd to revert a hexdump to ascii
noremap <Leader>H :%!xxd -c 32 -r<Enter>

" Map <Leader>d to insert the date as a markdown header
noremap <Leader>d i##<Esc>:r!date<CR>kJ
